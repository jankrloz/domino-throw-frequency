var spinner;
$(document).ready(function($) {

	$('#numTiros').mask('9?99999');

	$('#un_dado').click(function(event) {
		$('#undado').attr('checked', 'checked');
		$('#numTiros').focus();
	});
	$('#dos_dados').click(function(event) {
		$('#dosdados').attr('checked', 'checked');
		$('#numTiros').focus();
	});

	$('#menu_list li').hover(function() {
		$(this).animate({
			'backgroundColor': '#E9E9E9',
			'color': 'black'}, "fast"
		);
	}, function() {
		$(this).animate({
			'backgroundColor': 'transparent',
			'color': 'white'}, "fast"
		);
	});
	$('#start').hover(function() {
		$(this).animate({
			'backgroundColor': '#00C7DD',
			}, "fast"
		);
	}, function() {
		$(this).animate({
			'backgroundColor': '#099CAC',
			}, "fast"
		);
	});
	$('#start').click(function(event) {
		var opts = {
		  lines: 9, // The number of lines to draw
		  length: 20, // The length of each line
		  width: 8, // The line thickness
		  radius: 26, // The radius of the inner circle
		  corners: 1, // Corner roundness (0..1)
		  rotate: 0, // The rotation offset
		  direction: 1, // 1: clockwise, -1: counterclockwise
		  color: '#000', // #rgb or #rrggbb or array of colors
		  speed: 2.2, // Rounds per second
		  trail: 60, // Afterglow percentage
		  shadow: true, // Whether to render a shadow
		  hwaccel: true, // Whether to use hardware acceleration
		  className: 'spinner', // The CSS class to assign to the spinner
		  zIndex: 2e9, // The z-index (defaults to 2000000000)
		  top: 'auto', // Top position relative to parent in px
		  left: 'auto' // Left position relative to parent in px
		};
		$(this).hide();
		var target = document.getElementById('load');
		spinner = new Spinner(opts).spin(target);
		Concurrent.Thread.create(tirar);
	});
	$('#about').click(function(event) {
		alert('Hecho por: Juan Carlos Navarrete Gordillo para la materia de Probabilidad y Estadística con la profesora Jazmin Adriana Juárez Ramírez');
	});
	$('#contact').click(function(event) {
		alert('jankrloz.navarrete@gmail.com');
	});
});

function aleatorio(){ 
   	numPosibilidades = 6 - 1 
   	return Math.round(Math.random()*numPosibilidades+parseInt(1));
} 

function tirar (){
	var suma;
	var porcentaje = 0;
	var num_tiros = parseInt($('#numTiros').val());
	var sumas1 = [0, 0, 0, 0, 0, 0];
	var sumas2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	var sumatotal = 0;
	var restatotal = 0;
	var mintotal = 0;
	$('#tabla, #freq, #grafica, #cargando').html('');
	$('#prom').html('');
	$('#tabla, #freq').hide();
	$('#cargando').show();
	if ($('#undado').is(':checked')) {
		$('<tr><th>Tiro</th><th>Valor</th></tr>').appendTo('#tabla');
		$('<tr><th>Valor</th><th>Frec.</th><th>Frec. Relativa</th></tr>').appendTo('#freq');
	}
	else if ($('#dosdados').is(':checked')) {
		$('<tr><th>Tiro</th><th>Valor</th><th>Sum</th><th>Res</th><th>Min</th></tr>').appendTo('#tabla');
		$('<tr><th>Valor</th><th>Frec.</th><th>Frec. Relativa</th></tr>').appendTo('#freq');
	}
	for (var i = 1; i <= num_tiros; i++) {
		if ($('#undado').is(':checked')) {
			var dado1 = aleatorio();
			switch(dado1) {
				case 1:
					sumas1[0]++;
					break;
				case 2:
					sumas1[1]++;
					break;
				case 3:
					sumas1[2]++;
					break;
				case 4:
					sumas1[3]++;
					break;
				case 5:
					sumas1[4]++;
					break;
				case 6:
					sumas1[5]++;
					break;
			}
			//console.log(i+' '+dado1);
			$('<tr><td>'+i+'</td><td>'+dado1+'</td></tr>').appendTo('#tabla');
		}
		else if ($('#dosdados').is(':checked')) {
			var dado1 = aleatorio();
			var dado2 = aleatorio();
			var suma = dado1 + dado2;
			var resta = Math.abs(dado1 - dado2);
			var min = Math.min(dado1, dado2);
			switch(suma) {
				case 2:
					sumas2[0]++;
					break;
				case 3:
					sumas2[1]++;
					break;
				case 4:
					sumas2[2]++;
					break;
				case 5:
					sumas2[3]++;
					break;
				case 6:
					sumas2[4]++;
					break;
				case 7:
					sumas2[5]++;
					break;
				case 8:
					sumas2[6]++;
					break;
				case 9:
					sumas2[7]++;
					break;
				case 10:
					sumas2[8]++;
					break;
				case 11:
					sumas2[9]++;
					break;
				case 12:
					sumas2[10]++;
					break;
				
			}
			sumatotal += suma;
			restatotal += resta;
			mintotal += min;
			//console.log(i+' ('+dado1+','+dado2+') '+suma);
			$('<tr><td>'+i+'</td><td>('+dado1+', '+dado2+')</td><td>'+suma+'</td><td>'+resta+'</td><td>'+min+'</td></tr>').appendTo('#tabla');
		}
		porcentaje += 100/num_tiros;
		$('#cargando').html(parseInt(porcentaje)+'%');
	}
	$('<p>Promedios<br>Suma: '+sumatotal/num_tiros+' Resta: '+restatotal/num_tiros+' Mínimo: '+mintotal/num_tiros+'</p>').appendTo('#prom');
	if ($('#undado').is(':checked')) {
		$('<tr><td>1</td><td>'+sumas1[0]+'</td><td>'+sumas1[0]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>2</td><td>'+sumas1[1]+'</td><td>'+sumas1[1]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>3</td><td>'+sumas1[2]+'</td><td>'+sumas1[2]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>4</td><td>'+sumas1[3]+'</td><td>'+sumas1[3]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>5</td><td>'+sumas1[4]+'</td><td>'+sumas1[4]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>6</td><td>'+sumas1[5]+'</td><td>'+sumas1[5]/num_tiros+'</td></tr>').appendTo('#freq');
		for (var i = 0; i < sumas1.length; i++) {
			sumas1[i] = sumas1[i]/num_tiros;
		}
		var plot1 = $.jqplot ('grafica', [sumas1]);
	}
	else if ($('#dosdados').is(':checked')) {
		$('<tr><td>2</td><td>'+sumas2[0]+'</td><td>'+sumas2[0]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>3</td><td>'+sumas2[1]+'</td><td>'+sumas2[1]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>4</td><td>'+sumas2[2]+'</td><td>'+sumas2[2]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>5</td><td>'+sumas2[3]+'</td><td>'+sumas2[3]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>6</td><td>'+sumas2[4]+'</td><td>'+sumas2[4]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>7</td><td>'+sumas2[5]+'</td><td>'+sumas2[5]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>8</td><td>'+sumas2[6]+'</td><td>'+sumas2[6]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>9</td><td>'+sumas2[7]+'</td><td>'+sumas2[7]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>10</td><td>'+sumas2[8]+'</td><td>'+sumas2[8]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>11</td><td>'+sumas2[9]+'</td><td>'+sumas2[9]/num_tiros+'</td></tr>').appendTo('#freq');
		$('<tr><td>12</td><td>'+sumas2[10]+'</td><td>'+sumas2[10]/num_tiros+'</td></tr>').appendTo('#freq');
		for (var i = 0; i < sumas2.length; i++) {
			sumas2[i] = sumas2[i]/num_tiros;
		}
		var plot1 = $.jqplot ('grafica', [sumas2]);
	}
	spinner.stop();
	$('#cargando').hide();
	$("html, body").animate({ scrollTop: '255px'});
	$('#start').show();
	$('#tabla, #freq').show();

}